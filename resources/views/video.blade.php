@extends('layout')


@section('content')

<style>
    body{
        background-color: black;
    }
    video {
    position: fixed; right: 0; bottom: 0;
    min-width: 100%; min-height: 100%;
    width: auto; height: auto; z-index: -100;
    background: url(polina.jpg) no-repeat;
    background-size: cover;
}
</style>

<p id='videContainer'></p>

<script>
    
    var channel = Echo.channel('my-channel');
    channel.listen('.my-event', function(data) {

        if (data.message == 'https://www.enustkat.com.tr/tr/hakkimizda') {
            $('#videContainer').fadeOut(500, function() {
                $('#videContainer').html("<video src='/video/1.m4v' id='player' autoplay='true'  muted='muted'></video>").fadeIn(500);
            });
        } else if (data.message == 'https://www.enustkat.com.tr/tr/isler') {
            $('#videContainer').fadeOut(500, function() {
                $('#videContainer').html("<video src='/video/2.m4v' id='player' autoplay='true'  muted='muted'></video>").fadeIn(500);
            });
        } 
    });  

</script>
@endsection
    


