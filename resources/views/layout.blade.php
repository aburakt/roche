<html>
    <head>
        <title>Roche Client</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href={{ asset('css/app.css') }}>
       
        <script src="https://cdn.plyr.io/3.5.10/plyr.js"></script>
        <script src="https://cdn.plyr.io/3.5.10/plyr.polyfilled.js"></script>

        <link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" />



        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>
        <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

        
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>