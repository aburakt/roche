@extends('layout')


@section('content')



        <div class="container form">
            <form id="comment-form" name="comment-form" action="{{ route('commentAdd') }}" method="POST">
                @csrf
                <div class="form-group">
                    <p class="innerTitle">The page which you are visiting now: <br><span class="inner"></span></p><br><br>
                    <p class="innerTitle">Visitor's Name<br>
                    <input type="text" class="form-control bg-warning border border-dark rounded-0 text-dark"  id="visitor" name="visitor" >
                </div>
                <div class="form-group">
                    <p class="innerTitle"> Range 1 </p>
                    <input type="range" min="1" max="100" value="50" class="slider form-control bg-warning border border-dark rounded-0 text-dark" id="range_1" name="range_1">
                </div>
                <div class="form-group">
                    <p class="innerTitle"> Range 2 </p>
                    <input type="range" min="1" max="100" value="50" class="slider form-control bg-warning border border-dark rounded-0 text-dark" id="range_2" name="range_2">
                </div>
                <div class="form-group">
                    <p class="innerTitle"> Range 3 </p>
                    <input type="range" min="1" max="100" value="50" class="slider form-control bg-warning border border-dark rounded-0 text-dark" id="range_3" name="range_3">
                </div>
                <div class="form-group">
                    <p class="innerTitle">Extra Information<br>
                    <textarea class="form-control mb-4 phone_us bg-warning phone_us border border-dark rounded-0 text-dark" id="extra_comment" name="extra_comment" rows="7"></textarea>
                </div>
                <input type="hidden" id="url_address" name="url_address"> 
                <button type="submit" class="btn btn-dark rounded-0" id="send">Send</button>
            </form>
    </div>


<script>
    $('.form').hide();
    var channel = Echo.channel('my-channel');
    channel.listen('.my-event', function(data) {
        $('.form').show();
        formsubmitEcho();
        $("#send").prop('disabled', false);
        $('.inner').html(data.message);
        $('#url_address').val(data.message);
    });  

    $("#comment-form").submit(function(e) {
        formsubmit(e);
    });

    function resetForm() {
        $('#range_1').val(50);
        $('#range_2').val(50);
        $('#range_3').val(50);
        $("#extra_comment").attr("value", ""); 
        $('textarea#extra_comment').val(" ");
        $('#extra_comment').val(" ");
        $('#extra_comment').text('');
        $('#extra_comment').empty();
    }

    function formsubmitEcho() {                

    var form = $("#comment-form");
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data)
        {
            resetForm();
        }
    });
    }

    function formsubmit(e) {                

        e.preventDefault(); 

        var form = $("#comment-form");
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
                $("#send").attr('disabled','disabled'); 
                
            }
        });
    }
</script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

@endsection
    

