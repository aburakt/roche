<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps = true;
    protected $fillable = ['visitor', 'range_1', 'range_2', 'range_3', 'extra_comment', 'url_address'];

}
