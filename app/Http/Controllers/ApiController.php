<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Url;
use App\Comment;
use App\Events\Event;
use App\Events\MyEvent;

class ApiController extends Controller
{

    public function urlAdd(Request $request){

        if (strpos($request->url, 'enustkat.com.tr') !== false) {

            $url = Url::updateOrCreate([
                'url' => $request->url, 
                ]);
            $url->save();
            event(new MyEvent($request->url));
            echo ($request->url);
        }
    }

    public function commentAdd(Request $request){
        
        $comment = Comment::updateOrCreate([
            'visitor' => $request->visitor, 
            'url_address' => $request->url_address,
            'range_1' => $request->range_1,
            'range_2' => $request->range_2,
            'range_3' => $request->range_3,
            'extra_comment' => $request->extra_comment
            ]);
        $comment->save();
    }

    public function form(){
        return view('form');
    }

    public function video(){
        return view('video');
    }

    public function current(){

        $urls = url::select('url')->latest()->take(5)->get();

        return response()->json($urls);
        
    }
}
