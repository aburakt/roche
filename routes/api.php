<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/urlAdd', 'ApiController@urlAdd');
Route::post('/commentAdd', 'ApiController@commentAdd')->name('commentAdd');
Route::get('/form', 'ApiController@form')->name('form');
Route::get('/video', 'ApiController@video')->name('video');
Route::get('/current', 'ApiController@current');
Route::get('/test', 'ApiController@test');